import java.io.Serializable;

public class Path implements Serializable {
    private String path;

    public Path(String path_) {
        this.path = path_;
    }

    public String getPath() {
        return path;
    }
}
