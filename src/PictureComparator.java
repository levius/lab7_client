import java.util.Comparator;
public class PictureComparator implements  Comparator<Picture> {
    @Override
    public  int compare(Picture p1, Picture p2){
        return p1.compareTo(p2);
    }

}
