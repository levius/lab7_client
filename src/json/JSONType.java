package json;

public enum JSONType {
    OBJECT("объект"),
    ARRAY("массив"),
    STRING("строка"),
    NUMBER("число"),
    BOOLEAN("логический");

    String russianName;

    JSONType(String russianName) {
        this.russianName = russianName;
    }

    public String getRussianName() {
        return russianName;
    }
}
