package json;

/**
 * An abstraction of json entity (object, array, any value)
 * @see JSONArray
 * @see JSONObject
 * @see JSONNumber
 * @see JSONBoolean
 * @see JSONString
 */
public class JSONEntity {
    protected JSONType type;

    /**
     * @return user-friendly representation of json
     */
    public final String toFormattedString() {
        return toFormattedString(2);
    }

    /**
     * @param tabSize desired size of tab
     * @return user-friendly representation of json
     */
    public final String toFormattedString(int tabSize) {
        return toFormattedString(tabSize, 0);
    }

    protected String toFormattedString(int tabSize, int depth) {
        return toString();
    }

    protected String getPaddingString(int tabSize, int depth) {
        StringBuilder padding = new StringBuilder();
        for (int i = 0; i < tabSize*depth; i++)
            padding.append(' ');
        return padding.toString();
    }

    public JSONType getType() {
        return type;
    }

    public String getStringType() {
        return getType().toString().toLowerCase();
    }

    public boolean isObject() {
        return type == JSONType.OBJECT;
    }

    public boolean isArray() {
        return type == JSONType.ARRAY;
    }

    public boolean isBoolean() {
        return type == JSONType.BOOLEAN;
    }

    public boolean isNumber() {
        return type == JSONType.NUMBER;
    }

    public boolean isString() {
        return type == JSONType.STRING;
    }

    public int toInt() throws Exception {
        return (int)toNumber().getValue();
    }

    public JSONObject toObject() throws Exception {
        return toObject(new IllegalStateException("Can't make object entity from " + getStringType() + " type"));
    }
    public JSONObject toObject(Exception onError) throws Exception {
        if (isObject())
            return (JSONObject)this;
        throw onError;
    }

    public JSONArray toArray() throws Exception {
        return toArray(new IllegalStateException("Can't make array entity from " + getStringType() + " type"));
    }
    public JSONArray toArray(Exception onError) throws Exception {
        if (isArray())
            return (JSONArray)this;
        throw onError;
    }

    public JSONBoolean toBoolean() throws Exception {
        return toBoolean(new IllegalStateException("Can't make boolean entity from " + getStringType() + " type"));
    }
    public JSONBoolean toBoolean(Exception onError) throws Exception {
        if (isBoolean())
            return (JSONBoolean) this;
        throw onError;
    }

    public JSONNumber toNumber() throws Exception {
        return toNumber(new IllegalStateException("Can't make number entity from " + getStringType() + " type"));
    }
    public JSONNumber toNumber(Exception onError) throws Exception {
        if (isNumber())
            return (JSONNumber) this;
        throw onError;
    }

    public JSONString toString(Exception onError) throws Exception {
        if (isString())
            return (JSONString) this;
        throw onError;
    }
}