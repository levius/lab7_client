package json;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents json object structure
 * (an entity with set of key-value entries)
 */
public class JSONObject extends JSONEntity {
    private HashMap<String, JSONEntity> items;

    {
        type = JSONType.OBJECT;
    }

    /**
     * Makes an empty json object
     */
    public JSONObject() {
        this(new HashMap<>());
    }

    /**
     * Makes a json object and fills it with content from given map
     * @param map map to fill json object from
     */
    public JSONObject(Map<? extends String, ? extends JSONEntity> map) {
        items = new HashMap<>(map);
    }

    /**
     * Makes a json object and puts given entry to it
     * @param entry entry to put to json object
     */
    public JSONObject(Map.Entry<String, JSONEntity> entry) {
        items = new HashMap<>();
        items.put(entry.getKey(), entry.getValue());
    }

    /**
     * @return items of the object
     */
    public HashMap<String, JSONEntity> getItems() {
        return items;
    }

    /**
     * Sets items of the object
     * @param items items to set
     */
    public void setItems(HashMap<String, JSONEntity> items) {
        this.items = items;
    }

    /**
     * @param key key of desired item
     * @return item by given key
     */
    public JSONEntity getItem(String key) {
        return items.get(key);
    }

    /**
     * Puts item to object
     * @param key   key of item
     * @param value value of item
     */
    public void putItem(String key, JSONEntity value) {
        items.put(key, value);
    }

    /**
     * Puts boolean item and automatically boxes it to {@link JSONBoolean}
     * @param key   key of item
     * @param value value of item
     */
    public void putItem(String key, boolean value) {
        items.put(key, new JSONBoolean(value));
    }

    /***
     * Puts double-type number and automatically boxes it to {@link JSONNumber}
     * @param key   key of item
     * @param value value of item
     */
    public void putItem(String key, double value) {
        items.put(key, new JSONNumber(value));
    }

    /**
     * Puts an item by given entry
     * @param entry entry to put to object
     */
    public void putItem(Map.Entry<String, JSONEntity> entry) {
        items.put(entry.getKey(), entry.getValue());
    }

    /**
     * Removes item by key
     * @param key key to remove by
     */
    public void removeItem(String key) {
        items.remove(key);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append('{');

        for (Map.Entry<String, JSONEntity> entry : items.entrySet())
            builder.append('"').append(entry.getKey()).append('"').append(": ").append(entry.getValue()).append(", ");

        if (builder.length() > 1)
            builder.delete(builder.length() - 2, builder.length());
        builder.append('}');
        return builder.toString();
    }

    @Override
    protected String toFormattedString(int tabSize, int depth) {
        StringBuilder builder = new StringBuilder();
        String padding = getPaddingString(tabSize, depth);
        String innerPadding = getPaddingString(tabSize, depth+1);
        builder.append(padding).append('{').append('\n');

        for (Map.Entry<String, JSONEntity> entry : items.entrySet()) {
            builder.append(innerPadding).append('"').append(entry.getKey()).append('"').append(": ");
            if (entry.getValue() instanceof JSONArray || entry.getValue() instanceof JSONObject)
                builder.append('\n').append(entry.getValue().toFormattedString(tabSize, depth + 2));
            else
                builder.append(entry.getValue());
            builder.append(",\n");
        }

        if (builder.length() > 1)
            builder.delete(builder.length() - 2, builder.length());
        builder.append('\n').append(padding).append('}');
        return builder.toString();
    }
}
