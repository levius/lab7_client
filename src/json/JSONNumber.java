package json;

/**
 * Represents number for json structure
 */
public class JSONNumber extends JSONEntity {
    private double value;

    {
        type = JSONType.NUMBER;
    }

    JSONNumber(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        if (value == (long)value)
            return String.format("%d", (long)value);
        else
            return String.format("%s", value);
    }

    @Override
    protected String toFormattedString(int tabSize, int depth) {
        return getPaddingString(tabSize, depth) + toString();
    }
}
