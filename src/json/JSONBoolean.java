package json;

/**
 * Represents boolean value for json structure
 */
public class JSONBoolean extends JSONEntity{
    private boolean value;

    {
        type = JSONType.BOOLEAN;
    }

    /**
     * Makes boolean object from given value
     */
    JSONBoolean(boolean value) {
        this.value = value;
    }

    /**
     * Makes boolean object by given string. Object is true only of given string is equals "true"
     */
    JSONBoolean(String value) {
        this.value = value.equals("true");
    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return Boolean.toString(value);
    }

    @Override
    protected String toFormattedString(int tabSize, int depth) {
        return getPaddingString(tabSize, depth) + toString();
    }
}