package json;

/**
 * Represents string value for json structure
 */
public class JSONString extends JSONEntity {
    private String content;

    {
        type = JSONType.STRING;
    }

    /**
     * Makes an empty string
     */
    public JSONString() {
        content = "";
    }

    /**
     * Makes a string with given content
     * param content
     */
    public JSONString(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return '"' + content + '"';
    }

    @Override
    protected String toFormattedString(int tabSize, int depth) {
        return getPaddingString(tabSize, depth) + toString();
    }
}
