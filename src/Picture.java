import java.io.Serializable;
import java.time.LocalDateTime;
public class Picture implements Comparable<Picture>, Serializable {
    protected String name;
    protected Creator creator;
    protected int artCOST;
    protected String Username;
    private String now;
    Picture( Creator _creator, String _name,String User) {
        artCOST = 0;
        creator = _creator ;
        name = _name;
        artCOST += this.getCreator().length()*6;
        Username = User;
        now = LocalDateTime.now().toString();
    }
    Picture( Creator _creator, String _name,String User, String date) {
        artCOST = 0;
        creator = _creator ;
        name = _name;
        artCOST += this.getCreator().length()*6;
        Username = User;
        now = date;
    }
    Picture(String _name,String User) {
        artCOST = 0;
        creator = new Creator("Картина неизвестного художника");
        name = _name;
        artCOST += this.getCreator().length()*6;
        Username = User;
        now = LocalDateTime.now().toString();
    }
    Picture( Creator _creator,String User) {
        artCOST = 0;
        creator = _creator ;
        name = "У этой картины нет названия";
        artCOST += this.getCreator().length()*6;
        Username = User;
        now = LocalDateTime.now().toString();
    }

    Picture(String User){
        name = "У этой картины нет названия";
        creator = new Creator("Картина неизвестного художника");
        Username = User;
        now = LocalDateTime.now().toString();
    }


    public String getCreator() {
        return creator.getCreatorSurname(creator);
    }
    public String getName() { return name; }
    public String getUsername(){return Username;}
    public String getDate(){return now;}
    @Override
    public String toString() {
        Sign data = new Sign(this);
        return "\nПод картиной надпись:\n"+data.toString();
    }
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.getCreator() == null) ? 0 : this.getCreator().length());
        return result;
    }
    @Override
    public  int compareTo(Picture p) {
        if (this.getName().length() > p.getName().length()) {
            return 1;
        }
        else if (this.getName().length() == p.getName().length()) {
            return 0;
        }
        else {
            return -1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }
        if (!(obj.getClass() == this.getClass())){
            return false;
        }
        Picture pic = (Picture) obj;
        if (this.getCreator().equals(pic.getCreator()) && this.getName().equals(pic.getName()) && this.getUsername().equals(pic.getUsername())){
            return true ;
        }
        return false;
    }
}
