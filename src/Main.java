import json.*;

import java.io.*;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.UnresolvedAddressException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

    private static String host = "localhost";
    private static int port = 1488;
    private static Socket s;
    private static String c;
    private static String login;
    private static String password;
    private static String email = "";
    private static String x;

    public static void main(String[] args) {
        try {
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    System.out.println("I'm die");
                } catch (Exception e) {
                    System.err.println("Your work don't save");
                }
            }));
            System.out.println("Добро пожаловать!");
            System.out.println("Введите номер порта");

            Scanner scanner = new Scanner(System.in);

            String str;
            while (true) {
                try {
                    Scanner sc = new Scanner(System.in);
                    port = sc.nextInt();
                    break;
                } catch (InputMismatchException e) {
                    System.out.println("Неверный формат порта");
                }
            }
            System.out.println("Если вы не зарегистрированы, введите register, затем введите логин и адрес электронной почты. Ваш пароль придет на указанную электронную почту.");
            System.out.println("Если у вас есть аккаунт, введите login, затем введите свой логин и пароль.");

            while (true) {
                str = scanner.nextLine().trim();
                if (str.equals("login")) {
                    System.out.println("Введите логин");
                    login = scanner.nextLine().trim();
                    System.out.println("Введите пароль");
                    Console cnsl = System.console();
                    if (cnsl != null) {
                        password = new String(cnsl.readPassword("Password:"));
                    } else {
                        password = scanner.nextLine().trim();
                    }
                    String log = sendCommand("login", "");

                    if (log.equals("FOUND")) {
                        System.out.println("Авторизация прошла успешно");
                        break;
                    } else if (log.equals("NOTFOUND")) {
                        System.out.println("Неверный логин или пароль");
                    } else if (log.equals("LOGINED")) {
                        System.out.println("Данный пользователь уже авторизован");
                    }


                    System.out.println("Введите login или register.");
                } else if (str.equals("register")) {
                    System.out.println("Введите логин");
                    login = scanner.nextLine().trim();
                    System.out.println("Введите адрес электронной почты. На него придет ваш пароль.");
                    email = scanner.nextLine().trim();
                    if (email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$")) {
                        String log = sendCommand("register", "");
                        if (log.equals("true")) {
                            System.out.println("Вы зарегистрированы");
                            break;
                        } else {
                            System.out.println("Пользователь с таким данными уже существует");
                            System.out.println("Введите login или register.");
                        }
                    } else System.out.println("Неверный формат ввода e-mail!");
                }
            }
        } catch (
                InputMismatchException e) {
            System.out.println("Неверный порт");
        } catch (
                NoSuchElementException e) {
            System.out.println("Введен недопустимый символ ctrl+D");
            sendCommand("exit","");
            System.out.println("Завершение работы");
            System.exit(-1);
        }
        System.out.println("Добро пожаловать " + login);
        System.out.println("Введите help, чтобы открыть перечень команд");
        while (true) {
            try {
                String[] ss = readAndParse();
                if (ss != null) {
                    switch (ss[0]) {

                        case "insert": {

                            if (ss.length < 3) {
                                System.out.println("Не хватает аргумента у команды insert");
                                break;
                            }

                            System.out.println(sendCommand("insert", Integer.parseInt(ss[1]), prepare(ss)));
                            break;
                        }
                        case "remove": {

                            if (ss.length < 2) {
                                System.out.println("Не хватает аргумента у команды remove");
                                break;
                            } else
                                System.out.println(sendCommand("remove", prepare(ss)));
                            break;
                        }
                        case "add": {
                            if (ss.length < 2) {
                                System.out.println("Не хватает аргумента у команды add");
                                break;
                            }
                            System.out.println(sendCommand("add", prepare(ss)));
                            break;
                        }
                        case "add_if_min": {
                            if (ss.length < 2) {
                                System.out.println("Нужен аргумент");
                                break;
                            }
                            System.out.println(sendCommand("add_if_min", prepare(ss)));
                            break;
                        }
                        case "show": {
                            if (ss.length > 1) {
                                System.out.println("Неверный формат команды show ");
                                break;
                            } else
                                System.out.println(sendCommand("show", ""));
                            break;

                        }
                        case "help":
                            System.out.println(sendCommand("help", ""));
                            break;
                        case "import": {
                            File f = new File(ss[1]);
                            StringBuilder b = new StringBuilder();
                            try {
                                BufferedReader buf = new BufferedReader(new FileReader(f));

                                while ((c = buf.readLine()) != null) {
                                    System.out.print(c);
                                    b.append(c);
                                }
                            } catch (IOException e) {
                                System.out.println("Ошибка!");
                            }
                            System.out.println(b.toString());
                            sendCommand("import", b.toString());
                            break;
                        }
                        case "info":
                            System.out.println(sendCommand("info", ""));
                            break;
                        case "host":
                            try {
                                System.out.println("Введите новый адрес");
                                Scanner sc1 = new Scanner(System.in);
                                host = sc1.next();
                                System.out.println("Адрес изменен на " + host);
                                break;
                            } catch (NullPointerException e) {
                                System.out.println("Адрес не задан!");
                                break;
                            }
                        case "port":
                            try {
                                System.out.println("Введите новый порт");
                                Scanner sc1 = new Scanner(System.in);
                                port = sc1.nextInt();
                                System.out.println("Порт изменен на " + port);
                            } catch (NullPointerException e) {
                                System.out.println("Порт не задан!");
                            }
                            break;
                        case "exit":
                            System.out.println(sendCommand("exit", ""));
                            System.out.println("Завершение работы");
                            System.exit(0);
                        default:
                            System.out.println("Такой команды не существует. Вы можете посмотреть список команд с помощью команды help.");
                    }
                }
            } catch (NullPointerException e) {
                System.out.println("Неверно указаны данные");

            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Произошло переполнение массива");
                e.printStackTrace();
            }
        }

    }

    private static String MessageHandler(Message message) {
        try {
            s = new Socket();
            s.connect(new InetSocketAddress(host, port));
            DataOutputStream outt = new DataOutputStream(s.getOutputStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);

            oos.writeObject(message);
            byte[] data = baos.toByteArray();
            oos.close();

            outt.write(data);
            outt.flush();

            DataInputStream d = new DataInputStream(s.getInputStream());
            byte[] dat = new byte[65535];
            d.read(dat);
            int l = 0;
            for (int i = dat.length - 1; i >= 0; i--) {// stream api !!!!
                byte by = dat[i];
                if (by != 0) {
                    l = i;
                    break;
                }
            }
            String x = new String(Arrays.copyOf(dat, l + 1));

            return x;
        } catch (UnresolvedAddressException e) {
            return "Не удалось определить адрес сервера";
        } catch (UnknownHostException e) {
            return "Ошибка подключения к серверу: неизвестный хост.";
        } catch (SecurityException e) {
            return "Нет разрешения на подключение, проверьте свои настройки безопасности";
        } catch (ConnectException e) {
            return "Нет соединения с сервером. .";
        } catch (InputMismatchException e) {
            return ("Неверный порт");
        } catch (IOException e) {
            e.printStackTrace();
            return "Ошибка ввода-вывода, обработка запроса прервана";
        } catch (IllegalArgumentException e) {
            return "порт аут оф ренж";

        }

    }


    private static String sendCommand(String name, Picture picture) {
        Message message = new Message<>(name, picture, login, password, email);
        String v = MessageHandler(message);
        return v;
    }

    private static String sendCommand(String name, String path) {
        Message message = new Message<>(name, path, login, password, email);
        String v = MessageHandler(message);
        return v;
    }

    private static String sendCommand(String name, int index, Picture picture) {

        Message message = new Message(name, index, picture, login, password, email);
        String v = MessageHandler(message);
        return v;
    }

    // метод prepare создает на основании fullCommand объект класса Picture
    private static Picture prepare(String[] Command) {

        if (Command[0].equals("remove") && Command.length == 1) {
            System.out.println("Недостаточно аргументов для команды remove");
            return null;
        }

        if (Command[0].equals("insert") || Command[0].equals("remove") || Command[0].equals("add") || Command[0].equals("add_if_min") || Command[0].equals("import")) {

            if (Command[0].equals("insert") && Command.length < 3) {
                System.out.println("Недостаточно аргументов для команды insert ");
                return null;
            }

            if ((Command[0].equals("add") || Command[0].equals("add_if_min") || Command[0].equals("remove") || Command[0].equals("import")) && Command.length < 2) {
                System.out.println("Недостаточно аргументов для одной из команд: import, remove, add, add_if_min");
                return null;
            }

            if (Command[0].equals("insert") && (Command[1].contains("{") || Command[1].contains("}"))) {
                System.out.println("Индекс не должен содержать фигурных скобок");
                return null;
            }

            if (Command[0].equals("insert") && (Integer.parseInt(Command[1]) < 0)) {
                System.out.println("Индекс  не может быть отрицательным");
                return null;
            }

            if (Command[0].equals("add") || Command[0].equals("add_if_min") || Command[0].equals("remove") || Command[0].equals("insert")) {
                String element;
                if (Command[0].equals("insert")) {
                    element = Command[2].trim();

                } else {
                    element = Command[1].trim();
                }
                try {
                    element = element.replaceAll(" ", "");
                    JSONEntity entity = JSONParser.parse(element);

                    if (!entity.isObject()) {
                        System.out.println("Данный json должен быть объектом, но имеет тип " + entity.getType().getRussianName());
                        return null;
                    }

                    JSONObject object = (JSONObject) entity;
                    JSONEntity nameEntity = object.getItem("name");
                    JSONEntity creatorEntity = object.getItem("creator");
                    String name = "";
                    String creator = "";

                    if (nameEntity != null) {
                        if (nameEntity.isString()) {
                            name = ((JSONString) nameEntity).getContent();
                        } else {
                            System.out.println("Name должно быть строкой, но имеет тип " + nameEntity.getType().getRussianName());
                            return null;
                        }
                    }

                    if (creatorEntity != null) {
                        if (creatorEntity.isString()) {
                            creator = ((JSONString) creatorEntity).getContent();
                        } else {
                            System.out.println("Сreator должен быть строкой, но имеет тип " + creatorEntity.getType().getRussianName());
                            return null;
                        }
                    }
                    return new Picture(new Creator(creator), name, login);

                } catch (NullPointerException ex) {
                    System.out.println("Ошибка, элемент задан неверно");
                    return null;
                } catch (JSONParseException e) {
                    System.out.println(element);
                    System.out.println("Не удалось преобразовать строку в объект");
                    return null;
                }

            }
        }
        return null;
    }

    // метод readAnsParse считывает команду в json и в зависимости от того какая это команда заполняет нужным образом fullCommand
    private static String[] readAndParse() {
        String[] fullCommand;
        try {
            Scanner scanner = new Scanner(System.in);
            String command = "";
            do command = command + scanner.nextLine();
            while ((command.length() - command.replaceAll("}", "").length()) < (command.length() - command.replaceAll("\\{", "").length()));
            //ввели пока строчка не содержит равное количество скобочек

            command = command.trim();

            int put = command.indexOf("{");
            try {
                command = (new StringBuilder(command)).insert(put, " ").toString();
            } catch (IndexOutOfBoundsException e) {
                command.trim();
            }


            if (!command.split(" ", 2)[0].equals("insert")) {

                fullCommand = command.split(" ", 2); // делим по первому пробелу

                if (fullCommand.length > 1)
                    while (fullCommand[1].contains("  ")) fullCommand[1] = fullCommand[1].replaceAll("  ", " ");
                {
                    if (fullCommand[0].equals("import")) {

                        if (fullCommand.length > 1) {
                            fullCommand[1] = fullCommand[1].replaceAll(" ", "");
                            System.out.println(fullCommand[1]);
                        }
                    }
                }
            } else {

                if (command.split(" ", 3).length < 3)
                    fullCommand = command.split(" ", 2);

                else {
                    fullCommand = command.split(" ", 3);
                    while (fullCommand[1].contains("  ")) fullCommand[1] = fullCommand[1].replaceAll("  ", " ");
                }
            }

        } catch (NoSuchElementException e) {
            System.out.println("Введён недопустимый симво Ctrl+D");
            sendCommand("exit","");
            System.out.println("Завершение работы");
            System.exit(-1);
            return null;
        }
        return fullCommand;
    }
}