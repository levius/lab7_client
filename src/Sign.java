import java.time.LocalDateTime;

public class Sign {

    protected String _creator;
    protected String _name;
    protected String _username;
    private String data;

    Sign(Picture picture) {

        set_creator(picture.getCreator());

        set_name(picture.getName());

        set_username(picture.getUsername());
        set_date(picture.getDate());

    }

    @Override
    public String toString() {
        return this.get_name() + "\n" + this.get_creator()+"\n" + this.get_username() +"\n" +"дата создания " + this.get_date();
    }

    private void set_creator(String _creator) {
        this._creator = "   Художник " + _creator;
    }

    private void set_name(String _name) {
        this._name = "  Название картины " + _name;
    }
    private void set_username(String username) {
        this._username = "  Владелец картины " + username;
    }

    private void set_date(String _n) {
        this.data =  _n;
    }

    public String get_creator() {
        return _creator;
    }

    public String get_date(){return data.toString();}

    public String get_name(){
        return _name;
    }

    public String get_username(){return _username;}
}



