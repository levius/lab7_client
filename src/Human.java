import java.io.Serializable;

public class Human implements Serializable {
    private String surNAME;

    Human() {
        System.out.println("Вы создали человека без фамилии!!!");
    }

    Human(String _surNAME) {
        surNAME = _surNAME;
    }


    public String GetsurNAME() {
        return surNAME;
    }


    @Override
    public String toString() {
        return "\n Человек" + this.GetsurNAME();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Human visitor = (Human) obj;
        if (obj.hashCode() == this.hashCode())
            return true;
        return visitor.GetsurNAME() == this.GetsurNAME() && visitor.GetsurNAME() == this.GetsurNAME();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.GetsurNAME() == null) ? 0 : this.GetsurNAME().length());
        return result;
    }
}
